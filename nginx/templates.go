package nginx


import (
    "text/template"
)


var TplConfigNginx = template.Must(
    template.New("").Parse(`
        upstream project {
                {{range $server := .Bal}}
                    server {{$server.HostName}}:{{$server.Port}};
                {{end}}
        }

        server {
                listen 80;
                location / {
                        proxy_pass http://project;
                }


        }


        server {
            listen 8888;
            location /status {
                # Turn on stats
                stub_status on;
                access_log   off;
                allow all;
            }
        }
`))
