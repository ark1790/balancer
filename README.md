# Motivator

Motivator is a automatic scaling script written in `Golang` to motivate dynamic vertical scaling.

Scaling up and down depends on the number of available CPU cores and Physical Memory. The requirement config file `motivator.conf` is automatically created if it is not currently available in the working directory.



### Packages Dependencies

Executing this project from source requires the following packages in the $GOPATH

1. "github.com/naoina/toml"
2. "github.com/shirou/gopsutil/mem"

Use `go get <package-name>` to install them manually.


### Generating Binary

Use `go build cmd/main.go` to build the binary script.

### Running the project from source

Use `go run cmd/main.go` to run the script from source.
