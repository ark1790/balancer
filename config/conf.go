package conf

import(
    "os"
    "github.com/naoina/toml"
)

var Curr = struct {
    System struct {
        Host        string
        Directory   string
    }
    Limit struct{
        MaxConnections  uint
    }
    Worker struct {
        Requirements struct {
            CPU     uint
            Memory  uint64
        }
        App struct{
            Directory   string
            Ports       []int
        }
    }
    Nginx struct {
        Dir         string
    }
}{}

func LoadFile(name string) error{
    ff , err := os.Open(name)

    if os.IsNotExist(err) {
        ff , err = os.Create(name)
        if err != nil {
            return err
        }

        err = toml.NewEncoder(ff).Encode(Curr)
        if err != nil {
            return err
        }

        err = ff.Close()
        if err != nil{
            return err
        }

        return nil
    }

    if err != nil {
        return err
    }

    err = toml.NewDecoder(ff).Decode(&Curr)
    if err != nil {
        return err
    }

    return nil
}

func init() {
    Curr.System.Host = ":8998"
    Curr.Worker.App.Directory = "/home/ark/Documents/celica/platform/platform/"
    Curr.Worker.App.Ports = []int{8333 , 8334}
    Curr.Worker.Requirements.CPU = 1
    Curr.Worker.Requirements.Memory = 1024*1024
    Curr.Limit.MaxConnections = 1000
    Curr.Nginx.Dir = "/etc/nginx/sites-available/"
}
