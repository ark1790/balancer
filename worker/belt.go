package worker


import (

    "log"
    "time"
    "strings"
    "strconv"
    "runtime"
    "net/http"
    "io/ioutil"
    "github.com/shirou/gopsutil/mem"
    "github.com/ark1790/distributor/config"
    "github.com/ark1790/distributor/nginx"
)


func Run(){
    var t uint = 0
    log.Println("Initializing Belt...")
    for  {

        count := hit()
        mxCon := conf.Curr.Limit.MaxConnections

        var requiredWorker uint = 0
        requiredWorker = (count/mxCon)

        if count % mxCon != 0 {
            requiredWorker += 1
        }

        if requiredWorker <= 1 {
            requiredWorker = 2
        }

        v, _ := mem.VirtualMemory()
        


        if requiredWorker > WorkerCount  && (WorkerCount+conf.Curr.Worker.Requirements.CPU) < uint(runtime.NumCPU()) && v.Free > conf.Curr.Worker.Requirements.Memory {
            DeployWorker(&BalancerList[WorkerCount])
            UpdateNginxConfig(WorkerCount)
            nginx.Reload()
        }


        if requiredWorker < WorkerCount && requiredWorker > 2  &&  (t % 100 == 0) {
            StopWorker(&BalancerList[WorkerCount-1])
            UpdateNginxConfig(WorkerCount)
            nginx.Reload()
        }

        t = (t + 1)%500
        time.Sleep(1 * time.Second)
    }

}

func hit() uint{
    resp , err := http.Get("http://localhost:8888/status")
    if err != nil {
        panic(err)
    }

    defer resp.Body.Close()

    body , err := ioutil.ReadAll(resp.Body)

    if err != nil {
        log.Println(err)
        panic(err)
    }

    Body := string(body[:])
    Lines := strings.Split(Body , `\n`)
    Lines1 := strings.Split(Lines[0] , ` `)

    conn , err := strconv.Atoi(Lines1[2])

    if err != nil {
        log.Println(err)
        panic(err)
    }

    return uint(conn)
}
