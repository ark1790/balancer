package worker

import (
    "os"
    "log"
    // "os/exec"
    "runtime"
    "strconv"
    "io/ioutil"
    "encoding/json"
    "path/filepath"
    "github.com/ark1790/distributor/config"
    "github.com/ark1790/distributor/nginx"
)

type Balancer struct{
    Name        string
    Port        int
    Filename    string
    HostName    string
}

var Server = struct {
    Apps    []struct{
        Name            string `json:"name"`
        Script          string `json:"script"`
        Log_date_format string `json:"log_date_format"`
        Env  struct {
            NODE_ENV    string `json:"NODE_ENV"`
            Port        string `json:"PORT"`
        } `json:"env"`
    } `json:"apps"`
}{}



func UpdateNginxConfig(count uint) {

    newList := make([]Balancer , count)
    copy(newList , BalancerList[0:count])

    f , err := os.Create(filepath.Join(conf.Curr.Nginx.Dir , "default"))

    err = nginx.TplConfigNginx.Execute( f , struct{
        Bal []Balancer
    }{
        Bal: newList,
    })

    if err != nil {
        log.Print()
    }
}

var BalancerList [] Balancer


func Init() {

    Directory := conf.Curr.Worker.App.Directory
    BalancerList = make([]Balancer , runtime.NumCPU() - 1 )

    data , err := ioutil.ReadFile( filepath.Join( Directory , "server.json") )

    if err != nil {
        log.Print(err)
        panic(err)
    }


    err = json.Unmarshal(data , &Server)
    if err != nil {
        panic(err)
    }

    newServer := Server
    servername := "name"
    servername = Server.Apps[0].Name

    for i := 0 ; i <  runtime.NumCPU() - 1 ;i++{

        newServer.Apps[0].Name = servername + "-" + strconv.Itoa(i)
        newServer.Apps[0].Env.Port = strconv.Itoa(8333+i)
        BalancerList[i] = Balancer{newServer.Apps[0].Name , 8333+i , "server-" + strconv.Itoa(i) +".json" , "localhost"}

        b , err := json.MarshalIndent(newServer , " " , " ")
        if err != nil {
            log.Print(err)
            panic(err)
        }

        err = ioutil.WriteFile(filepath.Join(Directory , BalancerList[i].Filename) , b , 0777)
        if err != nil {
            log.Print(err)
            panic(err)
        }
        // DeployWorker(&BalancerList[i])
    }

    log.Println("Initialized Balancers")
}
