package worker


import (
    "os"
    "log"
    "os/exec"
    "path/filepath"
    "github.com/ark1790/distributor/config"
)

var WorkerCount uint = 0

func DeployWorker(b *Balancer) {
    Directory := conf.Curr.Worker.App.Directory

    C := exec.Command("pm2", "start" , filepath.Join(Directory , b.Filename) )

    C.Dir = Directory
    C.Stdout = os.Stdout
    C.Stderr = os.Stderr

    err := C.Run()
    log.Println("Deployed Worker" , WorkerCount)
    WorkerCount += 1
    if err != nil {
        log.Print(err)
    }

}

func StopWorker(b *Balancer) {
    Directory := conf.Curr.Worker.App.Directory

    C := exec.Command("pm2", "stop" , b.Name )

    C.Dir = Directory
    C.Stdout = os.Stdout
    C.Stderr = os.Stderr

    err := C.Run()
    log.Println("Stopped Worker" , WorkerCount)
    WorkerCount -= 1
    if err != nil {
        log.Print(err)
    }
}
