package main


import (
    "os"
    "log"
    "net/http"
    "os/signal"
    "github.com/ark1790/distributor/ui"
    "github.com/ark1790/distributor/config"
    "github.com/ark1790/distributor/worker"
)

func main() {


    err := conf.LoadFile("motivator.conf")
    if err != nil {
        log.Fatal(err)
    }

    worker.Init()
    go worker.Run()


    http.Handle("/", ui.Router)

    go func() {
        err := http.ListenAndServe(":8998" , nil)
        if err != nil {
            log.Fatal(err)
        }
    }()


    c := make(chan os.Signal, 1)
    signal.Notify(c , os.Interrupt)


    log.Printf("Received %s; exiting.." , <-c)
}
