package ui

import (
    "html/template"
)

var (
    TplLayout = template.Must(template.New("layout.html").ParseFiles("ui/templates/layout.html"))

    // Serve Index Page

    TplIndex = template.Must( template.Must(TplLayout.Clone()).ParseFiles("ui/templates/index.html") )


    // Serve New Balancer
    TplAddBalancer = template.Must( template.Must(TplLayout.Clone()).ParseFiles("ui/templates/addBalancer.html") )

)
