

package ui

import (
    "net/http"
    "github.com/gorilla/mux"
)

var Router = mux.NewRouter()

func ServeIndex(w http.ResponseWriter , r *http.Request) {
    err := TplIndex.Execute(w , "")
    if err != nil {
        panic(err)
    }
}


func ServeAddBalancer(w http.ResponseWriter , r *http.Request){
    err := TplAddBalancer.Execute(w , "")
    if err != nil {
        panic(err)
    }
}

func init(){
    Router.NewRoute().
        Methods("GET").
        Path("/").
        Handler(http.HandlerFunc(ServeIndex))

    Router.NewRoute().
        Methods("GET").
        Path("/add").
        Handler(http.HandlerFunc(ServeAddBalancer))
}
